<?php
//if(($_SERVER["HTTPS"] != "on") && (!headers_sent()))
//{
//    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
//    exit();
//}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Calculate Your Retirement!</title>
<link rel="stylesheet" type="text/css" href="./style.css">
</head>
<body>    
<?php
function getPage() {
//    if(isset($_SERVER["HTTPS"]) && strtolower($_SERVER["HTTPS"]) == "on"){
//        return 'https://'. $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
//    }else{
        return 'http://'. $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
//    }
}

// matches SEC calculator: https://www.investor.gov/additional-resources/free-financial-planning-tools/compound-interest-calculator
// Balance(Y)   =   P(1 + r)^Y   +   c[((1 + r)^Y - 1) / r]
function interest($investment,$contrib,$year,$in_rate=7){
    $rate = $in_rate / 100;
    $full_rate = pow($rate + 1, $year);
    $yearly_contrib = $contrib * 12;
    $total = $investment * $full_rate;

    $contrib_rate = (pow($rate + 1, $year) - 1) / $rate;
    $contrib_int = $yearly_contrib * $contrib_rate;
    $total += $contrib_int;
    
    return (double)$total;
}

// matches: http://financeformulas.net/Present_Value.html
// present_value = future_value * ( 1 / (( 1 + r ) ^ Y) )
function adjust_for_inflation($amount, $years, $inflation_rate=2.5){
    $int_rate = 1 + ($inflation_rate / 100);
    $rate = pow($int_rate , $years);
    $inverse_rate = 1/$rate;
    return (double)$inverse_rate * $amount;
}

function monthly_distribution($amount, $years){
    return (double)$amount/($years*12);
}

function interest_minus_fees($investment,$contrib,$year,$in_rate, $fee_percent, $fee_dollar){
    $interest_minus_percent_fees = interest($investment, $contrib, $year, ($in_rate - $fee_percent));
    $total_fee_dollar = $year * $fee_dollar;
    $total_fees = $interest_minus_percent_fees - $total_fee_dollar;
    return (double)$total_fees;
}

/*
2018 Tax Rates:
Rate            Unmarried               Married
10%             $0 - $9,524              $0 - $19,049
12%             $9,525 - $38,699         $19,050 - $77,399
22%             $38,700 - $82,499        $77,400 - $164,999
24%             $82,500 - $157,499       $165,000 - $314,999
32%             $157,500 - $199,999      $315,000 - $399,999
35%             $200,000 - $499,999      $400,000 - $599,999
37%             $500,000 - up            $600,000 - up
*/
function calculate_taxes($amount, $years, $married = true){
    $yearly_distribution = $amount / $years;

    if($married == true){
        if($yearly_distribution < 19049){ return $amount*0.9; }
        if($yearly_distribution < 77399){ return $amount*0.88; }
        if($yearly_distribution < 164999){ return $amount*0.78; }
        if($yearly_distribution < 314999){ return $amount*0.76; }
        if($yearly_distribution < 399999){ return $amount*0.68; }
        if($yearly_distribution < 599999){ return $amount*0.65; }else{ return $amount*0.63; }
    }else{
        if($yearly_distribution < 9524){ return $amount*0.9; }
        if($yearly_distribution < 38699){ return $amount*0.88; }
        if($yearly_distribution < 82499){ return $amount*0.78; }
        if($yearly_distribution < 157499){ return $amount*0.76; }
        if($yearly_distribution < 199999){ return $amount*0.68; }
        if($yearly_distribution < 499999){ return $amount*0.65; }else{ return $amount*0.63; }
    }
}

function monthly_ssa_inflation($ssa_amount, $years_in_retirement, $inflation_rate){
    $amount = $ssa_amount * 12 * $years_in_retirement;
    
    if($inflation_rate > 1.66){
        $total_after_inflation = adjust_for_inflation($amount, $years_in_retirement, ($inflation_rate - 1.66));
        return (double)(($total_after_inflation / $years_in_retirement) / 12);
    }else{
        if(($inflation_rate == 0) || ($inflation_rate == 1.66)){
            return $ssa_amount;
        }else{
            $total_after_deflation = interest($amount, 0, $years_in_retirement, (1.66 - $inflation_rate));
            return (double)(($total_after_deflation / $years_in_retirement) / 12);
        }
    }
}

?>


<div id="calc_content">
<h3>A REAL Retirement Calculator</h3>

<?php
$initial_pretax=0;
$initial_posttax=0;
$years_until=20;
$years_after=35;
$market_rate=7;
$inflation_rate=2.5;
$contrib_pretax=100;
$contrib_posttax=100;
$target=0;
$fees_dollar_pretax=0;
$fees_dollar_posttax=0;
$fees_percent_pretax=0;
$fees_percent_posttax=0;
$ssa = 0;

if (isset($_POST['initial_pretax'])){$initial_pretax=$_POST['initial_pretax'];}
if (isset($_POST['initial_posttax'])){$initial_posttax=$_POST['initial_posttax'];}
if (isset($_POST['contrib_pretax'])){$contrib_pretax=$_POST['contrib_pretax'];}
if (isset($_POST['contrib_posttax'])){$contrib_posttax=$_POST['contrib_posttax'];}
if (isset($_POST['years_until'])){$years_until=$_POST['years_until'];}
if (isset($_POST['years_after'])){$years_after=$_POST['years_after'];}
if (isset($_POST['market_rate'])){$market_rate=$_POST['market_rate'];}
if (isset($_POST['inflation_rate'])){$inflation_rate=$_POST['inflation_rate'];}
if (isset($_POST['ssa'])){$ssa=$_POST['ssa'];}
if (isset($_POST['target'])){$target=$_POST['target'];}
if (isset($_POST['married'])){$married=true;}else{ $married=false; }

if (isset($_POST['fees_dollar_pretax'])){$fees_dollar_pretax=$_POST['fees_dollar_pretax'];}
if (isset($_POST['fees_dollar_posttax'])){$fees_dollar_posttax=$_POST['fees_dollar_posttax'];}
if (isset($_POST['fees_percent_pretax'])){$fees_percent_pretax=$_POST['fees_percent_pretax'];}
if (isset($_POST['fees_percent_posttax'])){$fees_percent_posttax=$_POST['fees_percent_posttax'];}

?>
<form method="POST" action="<?php echo getPage(); ?>#results">

<p>Before tax (401k/traditional) already in account: <?php echo '<input type="number" name="initial_pretax" value='. $initial_pretax.' required/>'?> </p>
<p>After tax (ROTH IRA/ROTH 401k) already in account: <?php echo '<input type="number" name="initial_posttax" value='. $initial_posttax.' required/>'?> </p>

<p>Before tax monthly contribution (include your employer's match): <?php echo '<input type="number" name="contrib_pretax" value='. $contrib_pretax.' step="0.01" min="0" required/>'?> </p>
<p>After tax monthly contribution (include your employer's match): <?php echo '<input type="number" name="contrib_posttax" value='. $contrib_posttax.' step="0.01" min="0" required/>'?> </p>


<p>Before tax $/year fees: <?php echo '<input type="number" name="fees_dollar_pretax" value='. $fees_dollar_pretax.' step="0.01" min="0" required/>'?> &nbsp; %/year fees: <?php echo '<input type="number" name="fees_percent_pretax" value='. $fees_percent_pretax.' step="0.01" min="0" required/>'?> </p>
<p>After tax $/year fees: <?php echo '<input type="number" name="fees_dollar_posttax" value='. $fees_dollar_posttax.' step="0.01" min="0" required/>'?> &nbsp; %/year fees: <?php echo '<input type="number" name="fees_percent_posttax" value='. $fees_percent_posttax.' step="0.01" min="0" required/>'?> </p>

<hr style="width: 60%;">

<p>Years until Retirement*: <?php echo '<input type="number" name="years_until" value='. $years_until.' min="1" required/>' ?></p>

<p>Estimated Years in Retirement: <?php echo '<input type="number" name="years_after" value='. $years_after.' min="1" required/>' ?></p>

<p><input type="checkbox" name="married" checked> Married through retirement? (this changes your taxes)</p>

<hr style="width: 60%;">

<p>Estimated Yearly Market Gain/Return: <?php echo '<input type="number" name="market_rate" value='.$market_rate.' />' ?> % </p>

<p>Average Inflation Rate: <?php echo '<input type="number" name="inflation_rate" value='.$inflation_rate.' step="0.5" min="0" max="10" />'?> % </p>

<hr style="width: 60%;">

<p>Social Security Monthly Estimate (<a href="https://www.ssa.gov/benefits/retirement/estimator.html" target="_blank">Offical SSA Estimate</a> or <a href="https://smartasset.com/retirement/social-security-calculator" target="_blank">Unofficial Estimate</a>):  $ <?php echo '<input type="number" name="ssa" value='. $ssa.' required/>'?> </p> 

<p> <input type="submit" name="submit" value="Calculate"/> </p>
</form>

<br>

<?php
// IF form was submitted
if (isset($_POST['submit'])){
    setlocale(LC_MONETARY, 'en_US.UTF-8');
    $pretax_int = interest($initial_pretax,$contrib_pretax,$years_until,$market_rate);
    $posttax_int = interest($initial_posttax,$contrib_posttax,$years_until,$market_rate);
    
    $pretax_int_minus_fees = interest_minus_fees($initial_pretax,$contrib_pretax,$years_until,$market_rate, $fees_percent_pretax, $fees_dollar_pretax);
    $posttax_int_minus_fees = interest_minus_fees($initial_posttax,$contrib_posttax,$years_until,$market_rate, $fees_percent_posttax, $fees_dollar_posttax);
    
    $pretax_taxes = calculate_taxes($pretax_int_minus_fees, $years_after, $married);
    
    if($inflation_rate > 0){
        // AFTER TAXES!
        $pretax_inflation = adjust_for_inflation($pretax_taxes, $years_until, $inflation_rate);
        $posttax_inflation = adjust_for_inflation($posttax_int_minus_fees, $years_until, $inflation_rate);
    }else{
        $pretax_inflation = 'None set.';
        $posttax_inflation = 'None set.';
    }
?>
    <br><br><hr style="width: 80%;"><br><br>
    
    <h3 id="results">Results:</h3>
    <p>After <?php echo $years_until; ?> years, your account's values are:</p>

    <h4>Total Value:</h4>
    <div class="table_div">
    <table>
    <tr>
        <td>Account</td>
        <td>Total Value Before Fees</td>
        <td>Total Value After Fees</td>
        <td>After Taxes***</td>
        <td>Monthly amount in retirement</td>
    </tr>
    <tr>
        <td>Before Tax (401k/traditional)</td>
        <td><?php echo money_format('%.2n', $pretax_int); ?></td>
        <td><?php echo money_format('%.2n', $pretax_int_minus_fees); ?></td>
        <td><?php echo money_format('%.2n', $pretax_taxes); ?></td>
        <td><?php echo money_format('%.2n', monthly_distribution($pretax_taxes, $years_after)); ?></td>
    </tr>
    <tr>
        <td>After Tax (ROTH IRA/ROTH 401k)</td>
        <td><?php echo money_format('%.2n', $posttax_int); ?></td>
        <td><?php echo money_format('%.2n', $posttax_int_minus_fees); ?></td>
        <td>-Not Taxed-</td>
        <td><?php echo money_format('%.2n', monthly_distribution($posttax_int, $years_after)); ?></td>
    </tr>
    <tr>
        <td>Total</td>
        <td><?php echo money_format('%.2n', $pretax_int + $posttax_int); ?></td>
        <td><?php echo money_format('%.2n', $pretax_int_minus_fees + $posttax_int_minus_fees); ?></td>
        <td><?php echo money_format('%.2n', $pretax_taxes + $posttax_int_minus_fees); ?></td>
        <td><?php echo money_format('%.2n', monthly_distribution($posttax_int + $pretax_taxes, $years_after)); ?></td>
    </tr>
    </table>
    </div>
    
    <br><hr style="width: 60%;">
    See the difference compounding interest makes! <a href="https://www.investor.gov/additional-resources/free-financial-planning-tools/compound-interest-calculator" target="_blank">SEC Compound Interest Calculator</a>
    <br><hr style="width: 60%;"><br>
<?php if($inflation_rate > 0){ ?>    
    <h4>Adjusted for Inflation (in today's value):</h4>
    <div class="table_div">
    <table>
    <tr>
        <td>Account</td>
        <td>Total Value After Fees & Taxes<br>Adjusted for Inflation</td>
        <td>Monthly amount in retirement<br>Adjusted for Inflation</td>
    </tr>
    <tr>
        <td>Before Tax (401k/traditional)</td>
        <td><?php echo money_format('%.2n', $pretax_inflation); ?></td>
        <td><?php echo money_format('%.2n', monthly_distribution($pretax_inflation, $years_after)); ?></td>
    </tr>
    <tr>
        <td>After Tax (ROTH IRA/ROTH 401k)</td>
        <td><?php echo money_format('%.2n', $posttax_inflation); ?></td>
        <td><?php echo money_format('%.2n', monthly_distribution($posttax_inflation, $years_after)); ?></td>
    </tr>
    <tr>
        <td>Total</td>
        <td><?php echo money_format('%.2n', $pretax_inflation + $posttax_inflation); ?></td>
        <td><?php echo money_format('%.2n', monthly_distribution($pretax_inflation + $posttax_inflation, $years_after)); ?></td>
    </tr>
    </table>
    </div>
    
    <br><hr style="width: 60%;">
    Yes, the inflation numbers are correct! (I know it's a huge difference.)<br>Not planning for inflation can really mess up a retirement plan.<br>See inflation rates for other years here: <a href="https://smartasset.com/investing/inflation-calculator" target="_blank">Inflation Calculator</a> <br>
    <a href="https://www.vertex42.com/Calculators/inflation-calculator.html" target="_blank">Another Inflation Calculator</a>
    <br><hr style="width: 60%;"><br>
<?php } ?>    
    
    <h4>Monthly Totals:</h4>
    <div class="table_div">
    <table>
    <tr>
        <td>&nbsp;</td>
        <td>Before Tax Accounts</td>
        <td>After Tax Accounts</td>
        <td>Social Security</td>
        <td>Total per Month</td>
    </tr>
    <tr>
        <td>No Inflation</td>
        <td><?php echo money_format('%.2n', monthly_distribution($pretax_taxes, $years_after)); ?></td>
        <td><?php echo money_format('%.2n', monthly_distribution($posttax_int, $years_after)); ?></td>
        <td><?php echo money_format('%.2n', $ssa); ?></td>
        <td><?php echo money_format('%.2n', monthly_distribution($posttax_int + $pretax_taxes, $years_after) + $ssa); ?></td>
    </tr>
    <tr>
        <?php if($inflation_rate > 0){ ?>
        <td>Inflation Rate of <?php echo $inflation_rate; ?>%</td>
        <td><?php echo money_format('%.2n', monthly_distribution($pretax_inflation, $years_after)); ?></td>
        <td><?php echo money_format('%.2n', monthly_distribution($posttax_inflation, $years_after)); ?></td>
        <td><?php echo money_format('%.2n', monthly_ssa_inflation($ssa, $years_after, $inflation_rate)); ?>**</td>
        <td><?php echo money_format('%.2n', monthly_distribution($posttax_inflation + $pretax_inflation, $years_after) + monthly_ssa_inflation($ssa, $years_after, $inflation_rate)); ?></td>
        <?php } ?>
    </tr>
    </table>
    </div>
    
    <?php if($inflation_rate == 0){ ?>
    <p>However, inflation WILL eat a huge chunk out of this! To see what that amount will be worth in today's dollars, set the inflation rate above. 2.5% is the annual average for the United States.</p>
    <?php }else{ if($inflation_rate < 2.5){ ?>
    <p>However, the average inflation rate for the United States is 2.5%! You should be using that number or a higher one!</p>
    <?php }}
}
?>

    <br>&nbsp;<br><hr style="width: 60%;"><br>
    <p>*This calculator assumes you retire after you reach 59 1/2 years old. Retiring before this age will cause you to incur additional fees and taxes!</p>
    <p>**Social Security does adjust for inflation somewhat. However, their adjustments are often far below the actual inflation rate of that year. In the last 10 years, the Social Security Administration has adjusted for an average inflation rate of 1.66%. (<a href="https://www.ssa.gov/news/cola/" target="_blank">Source</a>)</p>
    <p>***Taxes are based on the 2018 tax rates.</p>

<br><br>&nbsp;<br><br>

</div>

<br><br>&nbsp;<br><br>

</body> </html> 
